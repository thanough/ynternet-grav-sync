---
title: 'La grande transition : d''un paradigme à l''autre'
summary: 'L''arrivée des outils numériques n''est pas une simple révolution technologique que nous pourrions regarder de loin sans nous sentir concernés. Une grande transition est à l’œuvre qui touche tous les aspects de notre vie quotidienne. Nous devons faire face à une évolution complexe et rapide des us et coutumes de notre civilisation.'
length: '10 minutes'
objectives:
    - 'Understand and explain Internet health in general terms.'
    - 'objectif 2'
    - 'objectif 3'
    - 'objectif 4'
audiences:
    - 'audience 1'
    - 'Beginner web user'
materials:
    - 'A compare and contrast chart like the one below'
skills:
    'Web Literacy Skills':
        url: 'https://learning.mozilla.org/web-literacy'
        subs:
            read:
                Syntesize: 'https://learning.mozilla.org/web-literacy/read/synthesize/'
            participate:
                Syntesize: 'https://learning.mozilla.org/web-literacy/read/synthesize/'
                Dance: 'https://learning.mozilla.org/web-literacy/read/dance/'
    '21st Century Skills':
        url: 'https://learning.mozilla.org/web-literacy'
        subs:
            read:
                Syntesize: 'https://learning.mozilla.org/web-literacy/read/synthesize/'
            participate:
                Syntesize: 'https://learning.mozilla.org/web-literacy/read/synthesize/'
                Dance: 'https://learning.mozilla.org/web-literacy/read/dance/'
image: 'TheysLaGrandeTransitiondeweb.jpg.883x1210_q85_box-0,0,883,1210_crop_detail.jpg'
---

# Voici le contenu du cours !

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.