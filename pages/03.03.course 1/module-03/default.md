---
title: 'La rupture technologique'
keyconcepts:
    'Société en métamorphose': 'http://netizen3.org/index.php/Soci%C3%A9t%C3%A9_en_m%C3%A9tamorphose'
    'Autre concept': 'http://netizen3.org/index.php/Soci%C3%A9t%C3%A9_en_m%C3%A9tamorphose'
---

La télévision ne pose pas problème en tant qu’outil technologique, ce qui indigne ses contradicteurs ce sont les intérêts qu'elle sert désormais. Parce qu’après une courte phase de soumission au pouvoir politique, le petit écran est passé sous le contrôle quasi exclusif des as du marketing, c’est-à-dire des prescripteurs de comportements que sont les publicitaires. Ces derniers suivent les théories issues du marketing américain, la « lifetime value ».

**Quand la Télé s'attaque au Subconscient**

<iframe width="560" height="315" src="https://www.youtube.com/embed/hi97JAz7UJ4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

&nbsp;

Finalement, les médias unidirectionnels et servant des intérêts privés tels que la plupart des radios, télévisions et journaux ne sont-ils pas voués à disparaître ? En 2015, cette question est au centre des préoccupations de la profession. Parallèlement, les médias multidirectionnels servant le bien commun ne sont-ils pas amenés à devenir la référence pour nos services d’information ?