---
title: 'Technologies de l''information: les grandes constantes'
summary: 'Retour au tapis de la souris. Après un tour des concepts de la mutation, ce chapitre plante le décor du monde numérique sous l''angle fonctionnel. Au début du XXIe siècle, utiliser internet peut paraître aussi compliqué que de conduire une voiture au début du XXe siècle ! Cherche chauffeur désespérément. Désolé M''sieur, ils sont tous pris, apprenez par vous-même...'
length: '10 minutes'
objectives:
    - 'Understand and explain Internet health in general terms.'
    - 'objectif 2'
    - 'objectif 3'
    - 'objectif 4'
audiences:
    - 'audience 1'
    - 'Beginner web user'
materials:
    - 'A compare and contrast chart like the one below'
skills:
    'Web Literacy Skills':
        url: 'https://learning.mozilla.org/web-literacy'
        subs:
            read:
                Syntesize: 'https://learning.mozilla.org/web-literacy/read/synthesize/'
            participate:
                Syntesize: 'https://learning.mozilla.org/web-literacy/read/synthesize/'
                Dance: 'https://learning.mozilla.org/web-literacy/read/dance/'
    '21st Century Skills':
        url: 'https://learning.mozilla.org/web-literacy'
        subs:
            read:
                Syntesize: 'https://learning.mozilla.org/web-literacy/read/synthesize/'
            participate:
                Syntesize: 'https://learning.mozilla.org/web-literacy/read/synthesize/'
                Dance: 'https://learning.mozilla.org/web-literacy/read/dance/'
---

Est-ce vraiment si difficile ? Pas nécessairement, à condition d'y aller pas à pas, d'apprendre tout au long de sa vie. Car la technologie évolue. Une remise à niveau est toujours nécessaire. Il convient avant tout de sortir des « produits » et de s'intéresser aux « fonctions ». Elles permettent d'entrevoir les constantes dans l'usage d'internet. Il existe en effet des repères communs, des concepts et un vocabulaire avec lesquels nous allons tenter de vous familiariser.